**UI-Automation**

_ui-test-automation-directory_ `robot -d REPORTS -v browser:edge TEST  <----- to run on edge browser`

`robot -d REPORTS -v browser:chrome TEST  <----- to run on google chrome browser`


**API-Automation**

_api-test-automation-directory_ `robot -d REPORTS TEST <-- run all test`
