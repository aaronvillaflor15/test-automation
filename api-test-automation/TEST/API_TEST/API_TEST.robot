***Settings***
Resource    ${EXECDIR}${/}RESOURCES${/}COMMON.robot

***Test Cases***
TC1: Verify GET Users request
    [Tags]  get_users
    ${res_get_users}    Get Users             ${GET_USERS_ENDPOINT}
    ${LENGTH_RES}       Get Length            ${res_get_users.json()}
    Status Should Be    ${HTTP_STATUS_200}
    Should Be Equal     ${LENGTH_RES}         ${INPUT_API_TEST['LENGTH']}

TC2: Verify GET User request by Id
     [Tags]  get_users_by_id   
     ${res_get_users_by_id}    Get Users By Id    ${GET_USERS_ENDPOINT}    ${INPUT_API_TEST['USER_ID']}
     Status Should Be          ${HTTP_STATUS_200}
     Should Be Equal           ${res_get_users_by_id.json()[0]["name"]}    Nicholas Runolfsdottir V

TC3: Verify POST Users request
    [Tags]  post_users 
    ${res_post_user}    Post User    ${POST_USER_ENDPOINT}     
    Status Should Be    ${HTTP_STATUS_201} 
    Should Be Equal     ${res_post_user.json()['id']}           ${REQ_POST_API_TEST['id']}
    Should Be Equal     ${res_post_user.json()['name']}         ${REQ_POST_API_TEST['name']}
    Should Be Equal     ${res_post_user.json()['username']}     ${REQ_POST_API_TEST['username']}
    Should Be Equal     ${res_post_user.json()['email']}        ${REQ_POST_API_TEST['email']}
    Should Be Equal     ${res_post_user.json()['phone']}        ${REQ_POST_API_TEST['phone']}
    Should Be Equal     ${res_post_user.json()['company']}      ${REQ_POST_API_TEST['company']}

     