***Keywords***
Get Users
    [Arguments]    ${get_users_endpoint}  
    ${res_get_users}    GET    ${API_BASE_PATH}${get_users_endpoint}
    ...                        expected_status=any
    [Return]    ${res_get_users}

Get Users By Id
    [Arguments]    ${get_users_by_endpoint}    ${arg_id_param}
    ${res_get_users_by_id}    GET    ${API_BASE_PATH}${get_users_endpoint}
    ...                      expected_status=any
    ...                      params=id=${arg_id_param}
    [Return]    ${res_get_users_by_id}

Post User
    [Arguments]    ${post_user_endpoint}  
    ${res_post_user}    POST    ${API_BASE_PATH}${post_user_endpoint}
    ...                         json=${REQ_POST_API_TEST}
    ...                         expected_status=any
    [Return]    ${res_post_user}
