*** Settings ***
Library    BuiltIn
Library    Collections
Library    FakerLibrary    WITH NAME    Faker
Library    RequestsLibrary
Library    String

#Variables Import
Variables  ${EXECDIR}${/}INPUT${/}API_TEST${/}api_test.py
Variables  ${EXECDIR}${/}INPUT${/}API_TEST${/}req_api_test.py


#Resource Import
Resource   ${EXECDIR}${/}RESOURCES${/}KEYWORDS${/}API_TEST${/}API_TEST.robot
Resource   ${EXECDIR}${/}CONFIG${/}URLS.robot
Resource   ${EXECDIR}${/}CONFIG${/}CONFIG_COMMON.robot
Resource   ${EXECDIR}${/}CONFIG${/}API_RESPONSE.robot









