***Settings***
Library              BuiltIn
Library              SeleniumLibrary
Library              Collections
Library              String
Library              FakerLibrary       WITH NAME       Fake

#Variables Import
Variables            ${EXECDIR}${/}RESOURCES${/}RESOURCE_MAPPING${/}PAGE_HOMEPAGE.py
Variables            ${EXECDIR}${/}RESOURCES${/}RESOURCE_MAPPING${/}PAGE_PAYEES.py
Variables            ${EXECDIR}${/}RESOURCES${/}RESOURCE_MAPPING${/}PAGE_PAYMENTS.py
Variables            ${EXECDIR}${/}INPUT${/}PAYMENTS${/}INPUT_PAYMENTS.py

#Resource Import
Resource             ${EXECDIR}${/}RESOURCES${/}KEYWORDS${/}KEYWORDS_COMMON.robot
Resource             ${EXECDIR}${/}RESOURCES${/}KEYWORDS${/}KEYWORDS_PAYEES.robot
Resource             ${EXECDIR}${/}RESOURCES${/}KEYWORDS${/}KEYWORDS_PAYMENTS.robot
Resource             ${EXECDIR}${/}CONFIG${/}URLS.robot
Resource             ${EXECDIR}${/}CONFIG${/}CONFIG_COMMON.robot
