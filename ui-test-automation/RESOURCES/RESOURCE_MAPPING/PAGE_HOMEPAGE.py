PAGE_HOMEPAGE = {
    "HAMBURGERMENU_MENU": "xpath://span[contains(text(),'Menu')]",
    "NAVBAR_PAYEES": "xpath://a[contains(@href,'/client/payees')]",
    "NAVBAR_PAY_OR_TRANSFER": "xpath://span[contains(text(),'Pay or transfer')]",
    "HOME_EVERYDAY_AMOUNT": "xpath://h3[text()='Everyday']/ancestor::div[@class='account-info']//span[@class='account-balance']",
    "HOME_BILLS_AMOUNT": "xpath://h3[@title='Bills ']//ancestor::div[@class='account-info']//span[@class='account-balance']",
}
