PAGE_PAYMENTS = {
    "BUTTON_FROM": "xpath://button[@data-monitoring-label='Transfer Form From Chooser']",
    "BUTTON_TO": "xpath://button[@data-monitoring-label='Transfer Form To Chooser']",
    "BUTTON_EVERYDAY": "xpath://p[text()='Everyday']/ancestor::button[@type='button']",
    "BUTTON_BILLS": "xpath://p[text()='Bills ']/ancestor::button[@type='button']",
    "TAB_ACCOUNTS": "xpath://span[contains(text(),'Accounts')]",
    "TEXTFIELD_AMOUNT": "name:amount",
    "BUTTON_TRANSFER": "xpath://button[@data-monitoring-label='Transfer Form Submit']",
    "SPIELS_TRANSFER_SUCCESSFUL": "xpath://span[@role='alert'][contains(text(),'Transfer successful')]"
}
