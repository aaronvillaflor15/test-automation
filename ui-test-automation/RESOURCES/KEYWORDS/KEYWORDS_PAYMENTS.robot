***Keywords***
Navigate To Payments page
    [Documentation]    Go to payments page
    Click Element    ${PAGE_HOMEPAGE["HAMBURGERMENU_MENU"]}
    Wait Until Element is Visible     ${PAGE_HOMEPAGE["NAVBAR_PAY_OR_TRANSFER"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
    Click Element    ${PAGE_HOMEPAGE["NAVBAR_PAY_OR_TRANSFER"]}
    Wait Until Element is Visible    ${PAGE_PAYMENTS["BUTTON_FROM"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
    Wait Until Element is Visible    ${PAGE_PAYMENTS["BUTTON_TO"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}