***Keywords***
Go To BNZ Website
    [Documentation]   Go to the homepage of BNZ website
    [Arguments]    ${browser}
    Open Browser    ${BNZ_BASE_URL}    browser=${browser}
    WAIT UNTIL ELEMENT IS VISIBLE    ${PAGE_HOMEPAGE["HAMBURGERMENU_MENU"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
    Maximize Browser Window

# Navigate To Payees page
#     [Documentation]   Go to payees page
#     Click Element    ${PAGE_HOMEPAGE["HAMBURGERMENU_MENU"]}
#     Wait Until Element is Visible     ${PAGE_HOMEPAGE["NAVBAR_PAYEES"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
#     Click Element    ${PAGE_HOMEPAGE["NAVBAR_PAYEES"]}
#     Wait Until Element is Visible    ${PAGE_PAYEES["TEXTLABEL_PAYEES"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
#     Wait Until Element is Visible    ${PAGE_PAYEES["SEARCHBAR_SEARCH_PAYEES"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}

Generate Account Number
    [Documentation]   Generate Account Number to be used
    [Arguments]
    ${account_number}=    Generate Random String    15    0123456789
    [Return]    ${account_number}

Generate Account Name
    [Documentation]  Generate account name to be used
    [Arguments]
    ${first_name}=    Fake.First Name Male
    ${last_name}=    Fake.Last Name Male
    ${full_name}=    Set Variable    ${first_name} ${last_name}   
    [Return]    ${full_name}

# Add New Payee
#     [Documentation]  Add new payees successfuly
#     Wait Until Element Is Visible    ${PAGE_PAYEES['BUTTON_ADD_PAYEES']}
#     Click Element    ${PAGE_PAYEES['BUTTON_ADD_PAYEES']}
#     Wait Until Element Is Visible    ${PAGE_PAYEES['TEXTFIELD_PAYEE_NAME']}
#     ${full_name}    Populate Payee Required Fields
#     [Return]    ${full_name}

# Populate Payee Required Fields
#     [Documentation]  Populate required fields of Payee forms
#     ${full_name}    Generate Account Name   
#     Input Text    ${PAGE_PAYEES['TEXTFIELD_PAYEE_NAME']}    ${full_name}
#     Press Keys    ${PAGE_PAYEES['TEXTFIELD_PAYEE_NAME']}    ${ENTER}
#     ${account_number}    Generate Account Number
#     Input Text    ${PAGE_PAYEES['TEXTFIELD_ACCOUNT_BANK']}    ${account_number} 
#     Press Keys    ${PAGE_PAYEES['TEXTFIELD_ACCOUNT_SUFFIX']}    ${ENTER}
#     Wait Until Element Is Enabled    ${PAGE_PAYEES['BUTTON_PROMPT_ADD']}    ${CONFIG_WAIT_ELEM_TIMEOUT}
#     Click Element     ${PAGE_PAYEES['BUTTON_PROMPT_ADD']}
#     # Wait Until Element Is Visible    ${PAGE_PAYEES['SPIELS_PAYEE_ADDED']}    ${CONFIG_WAIT_ELEM_TIMEOUT}
#     [Return]    ${full_name}

# Get Payees Name List From The Table
#     [Documentation]    Get text of all payees name and append to list                     
#     @{PAYEES_NAME}                      Get WebElements     ${PAGE_PAYEES['LIST_PAYEE_NAME']}
#     ${PAYEES_NAME_LIST}                 Create List
#     FOR     ${PAYEE_NAME}   IN      @{PAYEES_NAME}
#         ${TEXT}     Get Text        ${PAYEE_NAME}
#         Append To List      ${PAYEES_NAME_LIST}     ${TEXT}
#     END
#     [Return]    @{PAYEES_NAME_LIST}

# Navigate To Payments page
#     [Documentation]    Go to payments page
#     Click Element    ${PAGE_HOMEPAGE["HAMBURGERMENU_MENU"]}
#     Wait Until Element is Visible     ${PAGE_HOMEPAGE["NAVBAR_PAY_OR_TRANSFER"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
#     Click Element    ${PAGE_HOMEPAGE["NAVBAR_PAY_OR_TRANSFER"]}
#     Wait Until Element is Visible    ${PAGE_PAYMENTS["BUTTON_FROM"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}
#     Wait Until Element is Visible    ${PAGE_PAYMENTS["BUTTON_TO"]}    ${CONFIG_WAIT_ELEM_TIMEOUT}

Convert String to Number
    [Documentation]    Remove string and convert string to number                     
    [Arguments]    ${STRING}
    ${AMOUNT_STR}    Replace String    ${STRING}    ,    ${EMPTY}
    ${AMOUNT_NUM}    Convert to Number       ${AMOUNT_STR}
    [Return]    ${AMOUNT_NUM}
    