***Settings***
Resource         ${EXECDIR}${/}RESOURCES${/}COMMON.robot
Test Setup       Go To BNZ Website    ${browser}
Test Teardown    Close Browser

***Test Case***
TC1: Verify you can navigate to Payees page using the navigation menu
    [Tags]    payees   
    # Set Selenium Speed    .5
    # Set Selenium Speed    .5
    Navigate To Payees Page
    Location Should Be             ${BNZ_BASE_URL}/payees
    Page Should Contain Element    ${PAGE_PAYEES["TEXTLABEL_PAYEES"]}
    Page Should Contain Element    ${PAGE_PAYEES["SEARCHBAR_SEARCH_PAYEES"]} 

TC2: Verify you can add new payee in the Payees page
    [Tags]    payees    new_payee
    Navigate to Payees Page
    ${full_name}                        Add New Payee
    Wait Until Element Is Visible       ${PAGE_PAYEES['SPIELS_PAYEE_ADDED']}    ${CONFIG_WAIT_ELEM_TIMEOUT} 
    Click Element                       ${PAGE_PAYEES['COLUMN_LAST_PAID']}
    Element Should Contain              ${PAGE_PAYEES['LIST_LAST_ITEM']}        ${full_name}
   
TC3: Verify payee name is a required field
    [Tags]    payees    required_fields
    Navigate to Payees Page
    Wait Until Element Is Visible    ${PAGE_PAYEES['BUTTON_ADD_PAYEES']}
    Click Element                    ${PAGE_PAYEES['BUTTON_ADD_PAYEES']}
    Press Keys                       ${PAGE_PAYEES['TEXTFIELD_ACCOUNT_SUFFIX']}      ${ENTER}
    Click Element                    ${PAGE_PAYEES['BUTTON_DISABLED_PROMPT_ADD']}
    Page Should Contain Element      ${PAGE_PAYEES['SPIELS_PAYEE_NAME_REQUIRED']}
    ${full_name}                     Generate Account Name
    Input Text                       ${PAGE_PAYEES['TEXTFIELD_PAYEE_NAME']}          ${full_name}
    Press Keys                       ${PAGE_PAYEES['TEXTFIELD_PAYEE_NAME']}          ${ENTER}
    Click Element                    ${PAGE_PAYEES['BUTTON_DISABLED_PROMPT_ADD']}
    Page Should Contain Element      ${PAGE_PAYEES['SPIELS_BANK_CODE_REQUIRED']}
    Click Element                    ${PAGE_PAYEES['TEXTFIELD_ACCOUNT_BRANCH']}
    Page Should Contain Element      ${PAGE_PAYEES['SPIELS_BRANCH_CODE_REQUIRED']}
    Click Element                    ${PAGE_PAYEES['TEXTFIELD_ACCOUNT_ACCOUNT']}
    Page Should Contain Element      ${PAGE_PAYEES['SPIELS_ACCOUNT_NUMBER_REQUIRED']}
    Click Element                    ${PAGE_PAYEES['TEXTFIELD_ACCOUNT_SUFFIX']}
    Page Should Contain Element      ${PAGE_PAYEES['SPIELS_SUFFIX_REQUIRED']}
    Populate Payee Required Fields
    Wait Until Element Is Visible    ${PAGE_PAYEES['SPIELS_PAYEE_ADDED']}             ${CONFIG_WAIT_ELEM_TIMEOUT}

TC4: Verify that payees can be sorted by name
    [Tags]    payees    sort_by_name
    Navigate to Payees Page
    ${full_name}                     Add New Payee
    Wait Until Element Is Visible    ${PAGE_PAYEES['SPIELS_PAYEE_ADDED']}    ${CONFIG_WAIT_ELEM_TIMEOUT} 
    ${PAYEES_NAME_LIST}              Get Payees Name List From The Table
    Log To Console                   ${PAYEES_NAME_LIST} 
    @{SORT_PAYEES_NAME}              Copy List                               ${PAYEES_NAME_LIST} 
    Sort List                        ${SORT_PAYEES_NAME}     
    Lists Should Be Equal            ${SORT_PAYEES_NAME}                     ${PAYEES_NAME_LIST} 
    Log to console                   ${SORT_PAYEES_NAME}  
    Click Element                    ${PAGE_PAYEES['COLUMN_NAME']}
    ${PAYEES_NAME_LIST}              Get Payees Name List From The Table
    Reverse List                     ${SORT_PAYEES_NAME}  
    Lists Should Be Equal            ${SORT_PAYEES_NAME}                     ${PAYEES_NAME_LIST} 
    Log to console                   ${PAYEES_NAME_LIST}  
    
