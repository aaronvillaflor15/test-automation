***Settings***
Resource         ${EXECDIR}${/}RESOURCES${/}COMMON.robot
Test Setup       Go To BNZ Website    ${browser}
Test Teardown    Close Browser

***Test Case***
TC5: Navigate to Payments page
    [Tags]    payments
    Set Selenium Speed    .7
    ${CURRENT_EVERYDAY_AMOUNT_STR}=      Get Text                    ${PAGE_HOMEPAGE['HOME_EVERYDAY_AMOUNT']}
    ${CURRENT_BILLS_AMOUNT_STR}=         Get Text                    ${PAGE_HOMEPAGE['HOME_BILLS_AMOUNT']}
    ${CURRENT_EVERYDAY_AMOUNT_NUM}=      Convert String to Number    ${CURRENT_EVERYDAY_AMOUNT_STR}    
    ${CURRENT_BILLS_AMOUNT_NUM}=         Convert String to Number    ${CURRENT_BILLS_AMOUNT_STR}   
    ${EXPECTED_EVERYDAY_AMOUNT}=         Evaluate                    ${CURRENT_EVERYDAY_AMOUNT_NUM} - ${INPUT_PAYMENTS['AMOUNT']}
    ${EXPECTED_BILLS_AMOUNT}=            Evaluate                    ${CURRENT_BILLS_AMOUNT_NUM} + ${INPUT_PAYMENTS['AMOUNT']}
    Log to console                       Expected-Everyday:${EXPECTED_EVERYDAY_AMOUNT} 
    Log to console                       Expected-Bills:${EXPECTED_BILLS_AMOUNT}

    Navigate To Payments page
    Click Element                        ${PAGE_PAYMENTS['BUTTON_FROM']}
    Wait Until Element Is Visible        ${PAGE_PAYMENTS['BUTTON_EVERYDAY']}     ${CONFIG_WAIT_ELEM_TIMEOUT}
    Click Element                        ${PAGE_PAYMENTS['BUTTON_EVERYDAY']}
    Wait Until Element Is Visible        ${PAGE_PAYMENTS['BUTTON_TO']}           ${CONFIG_WAIT_ELEM_TIMEOUT}
    Click Element                        ${PAGE_PAYMENTS['BUTTON_TO']}
    Click Element                        ${PAGE_PAYMENTS['TAB_ACCOUNTS']}
    Wait Until Element Is Visible        ${PAGE_PAYMENTS['BUTTON_BILLS']}        ${CONFIG_WAIT_ELEM_TIMEOUT}
    Click Element                        ${PAGE_PAYMENTS['BUTTON_BILLS']}
    Wait Until Element Is Visible        ${PAGE_PAYMENTS['TEXTFIELD_AMOUNT']}    ${CONFIG_WAIT_ELEM_TIMEOUT}
    Input Text                           ${PAGE_PAYMENTS['TEXTFIELD_AMOUNT']}    ${INPUT_PAYMENTS['AMOUNT']}
    Click Element                        ${PAGE_PAYMENTS['BUTTON_TRANSFER']}
    Wait Until Element Is Visible        ${PAGE_PAYMENTS['SPIELS_TRANSFER_SUCCESSFUL']}     ${CONFIG_WAIT_ELEM_TIMEOUT}

    ${RUNNING_EVERYDAY_AMOUNT_STR}=      Get Text    ${PAGE_HOMEPAGE['HOME_EVERYDAY_AMOUNT']}
    ${RUNNING_BILLS_AMOUNT_STR}=         Get Text    ${PAGE_HOMEPAGE['HOME_BILLS_AMOUNT']}
    ${RUNNING_EVERYDAY_AMOUNT_NUM}=      Convert String to Number       ${RUNNING_EVERYDAY_AMOUNT_STR}    
    Log to console                       Actual-Everyday:${RUNNING_EVERYDAY_AMOUNT_NUM}
    ${RUNNING_BILLS_AMOUNT_NUM}=         Convert String to Number               ${RUNNING_BILLS_AMOUNT_STR}   
    Log to console                       Actual-Bills:${RUNNING_BILLS_AMOUNT_NUM}
    # VALIDATE Result
    Should Be Equal                      ${EXPECTED_EVERYDAY_AMOUNT}    ${RUNNING_EVERYDAY_AMOUNT_NUM}    
    Should Be Equal                      ${EXPECTED_BILLS_AMOUNT}       ${RUNNING_BILLS_AMOUNT_NUM}
    